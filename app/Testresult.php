<?php
namespace App;


use Illuminate\Database\Eloquent\Model;

/**
 * Class Role
 *
 * @package App
 * @property string $title
*/
class Testresult extends Model
{
   

    protected $table = 'test_result';
    
    protected $fillable = ['user_id','user_email','name','test_time','test_score','curectAns','wrongAns','uncurectAns'];

    
}
