<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class UserRequests
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        if(Auth::user()->role_id == 0 || (Auth::user()->role_id == 1 && Auth::user()->test_status != 1)) {
           return $next($request);
        }else{
            return redirect()->back();
        }
    }
}
