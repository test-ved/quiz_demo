<?php

/*
 * Taken from
 * https://github.com/laravel/framework/blob/5.3/src/Illuminate/Auth/Console/stubs/make/controllers/HomeController.stub
 */

namespace App\Http\Controllers;

use App\Http\Requests;
use Auth; 
use Illuminate\Http\Request;
use App\Questionnaires;
use App\User;
use App\Testresult;
use App\Tests;

/**
 * Class HomeController
 * @package App\Http\Controllers
 */
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return Response
     */

     public function login()
    {
        return view('adminlte::auth.login');
    }



    public function index()
    {

        $role_id = Auth::user()->role_id;
        $test_status = Auth::user()->test_status;
        $name = Auth::user()->name;
        $email = Auth::user()->email;
        if ($role_id == 1 && $test_status == 0)
        {
            return redirect(url('/assessment/user/assessment_test'));
        }
        else if($test_status == 1){
            return redirect(url('/unauthenticate'));
        }

        else {

            $question_count = Questionnaires::all()->count();
            $user_details  = User::select('name')->where('role_id','=','1')->get();
            $user_count = User::where('role_id','=','1')->count();
            $test_count = Testresult::all()->count();
            $test_details = Testresult::select('name','user_id','test_score')->orderBy('test_score','desc')->limit('5')->get();
            return view('adminlte::home')->with([
                
                'user_details' => $user_details,
                'question_count' => $question_count,
                 'user_count' => $user_count,
                 'test_count' => $test_count,
                 'test_details' => $test_details                
            ]);

        }
    }
}