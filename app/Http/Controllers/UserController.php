<?php

/*
 * Taken from
 * https://github.com/laravel/framework/blob/5.3/src/Illuminate/Auth/Console/stubs/make/controllers/HomeController.stub
 */

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request; 
use Illuminate\Support\Facades\Auth;
use App\Category;
use App\Questionnaires;
use App\AnswerOptions;
use Mail;
use App\User;
use App\Testresult;
use App\Tests;


/**
 * Class HomeController
 * @package App\Http\Controllers
 */
class UserController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return Response
     */
    public function user_dashboard()
    {
                $user_id  = Auth::user()->id;

                $apt_ques = Questionnaires::select('id')->where(['question_status' => 0])->where(['question_topic' => 1])->inRandomOrder()->limit('5')->get();
                $rea_ques = Questionnaires::select('id')->where(['question_status' => 0])->where(['question_topic' => 2])->inRandomOrder()->limit('5')->get();
                $mat_ques = Questionnaires::select('id')->where(['question_status' => 0])->where(['question_topic' => 3])->inRandomOrder()->limit('5')->get();

                for ($i=0; $i<5;$i++)
                {
                    $arr[$i] = $apt_ques[$i]['id'];

                }

                for ($i=0,$j=5; $i<5,$j<10;$i++,$j++)
                {
                    $arr[$j] = $rea_ques[$i]['id'];
                }

                for ($i=0,$j=10; $i<5,$j<15;$i++,$j++)
                {
                    $arr[$j] = $mat_ques[$i]['id'];
                }


             //     echo "<pre>"; print_r($ar); echo "</pre>";
             // $test_question = Questionnaires::select('id')->where(['question_status' => 0])->inRandomOrder()->get();
            

             //  $size = 15;
             //  $count = Questionnaires::select('id')->where(['question_status' => 0])->inRandomOrder()->count();



             // if ($size > $count)
             // {
             //    $size = $count;
             // } 



             // for ($i=0; $i<$size; $i++) { 
            
             //    $arr[$i] = $test_question[$i]['id'];
               

             // }


             // echo "<pre>"; print_r($arr); echo "</pre>";

                $rand_json = json_encode($arr);


                $rand_base = base64_encode($rand_json);

                $base = array(

                    'user_id' => $user_id,
                    'ques_test' => $rand_base
                );




         Tests::insert($base);




        return view('user_section.user_dashboard')->with([
            'user' => Auth::user(),
            'action' => 'add',
            'page_title' => 'Add Integration'
            ]);
    }

    public function test_view()
    {
        
        $test_question = Questionnaires::where(['question_status' => 0])->get()->toArray();



        // echo "<pre>"; print_r($test_question); echo "</pre>";




        $user_id  = Auth::user()->id;

        $test = Tests::select('ques_test')->where('user_id','=', $user_id)->get()->toArray();


       $ques_test = $test[0]['ques_test'];



        $ques_test = base64_decode($ques_test);

        $ques_test = json_decode($ques_test);    

         $i = 0; 
        


        foreach($ques_test as $t)
        {
            $test_q[$i] = Questionnaires::where('id','=',$t)->where(['question_status' => 0])->first()->toArray();

            $i++;
        }



       return view('user_section.test_view')->with([
            'user' => Auth::user(),
            'action' => 'add',
            'questions' => $test_q,
            'page_title' => 'Add Integration',
            'menu' => [
            'tree' => 'integrations',
            'item' => 'new-integration'
            ]
            ]);
    }

    public function test_submit()
    {           

        $user_id = auth()->user()->id;
        $user_email = auth()->user()->email;
        $user_name = auth()->user()->name;
        $start_time = $_POST['start_time'];
        $questionID = explode(",", $_POST['question_id']);
        $countID = sizeof($questionID);

        for ($i=0; $i < $countID; $i++) 
        { 
            $questionid = $questionID[$i];
            $checkoption = Questionnaires::where(['id' => $questionid])->get();
            foreach ($checkoption as $key) 
            {
                $question_type = $key->question_type;
                $correctOption = $key->is_correct;
            }

            // for objective type questions. 
            if($question_type == '1' || $question_type == '4'){

                if (isset($_POST[$questionid])) 
                {
                     if($correctOption == $_POST[$questionid]){

                            $questionid = $questionid;
                            $user_ans = $_POST[$questionid];
                            $is_correct = '1';
                            $option_status = '1';
                            $data = AnswerOptions::create([
                                'user_id' => $user_id,
                                'ques_id' => $questionid,
                                'user_option' => $_POST[$questionid],
                                'is_correct' => $is_correct,
                                'option_status' => $option_status,
                            ]);
                    }else{
                            $is_correct = '0';
                            $option_status = '1';
                            $data = AnswerOptions::create([
                                'user_id' => $user_id,
                                'ques_id' => $questionid,
                                'user_option' => $_POST[$questionid],
                                'is_correct' => $is_correct,
                                'option_status' => $option_status,
                            ]); 
                    }
                }else{
                            $is_correct = '0';
                            $option_status = '0';
                            $data = AnswerOptions::create([
                                'user_id' => $user_id,
                                'ques_id' => $questionid,
                                'user_option' => null,
                                'is_correct' => $is_correct,
                                'option_status' => $option_status,
                            ]); 
                }      
            }


             // for multiple  type questions. 
            if($question_type == '2'){
                $correctOption = json_decode(base64_decode($correctOption));   

                    if (isset($_POST[$questionid]))
                    {
                            if($correctOption == $_POST[$questionid]){

                                    $userAns = base64_encode(json_encode($_POST[$questionid]));
                                    $is_correct = '1';
                                    $option_status = '1';
                                    $data = AnswerOptions::create([
                                        'user_id' => $user_id,
                                        'ques_id' => $questionid,
                                        'user_option' => $userAns,
                                        'is_correct' => $is_correct,
                                        'option_status' => $option_status,
                                    ]);
                            }else{
                                    $userAns = base64_encode(json_encode($_POST[$questionid]));
                                    $is_correct = '0';
                                    $option_status = '1';
                                    $data = AnswerOptions::create([
                                        'user_id' => $user_id,
                                        'ques_id' => $questionid,
                                        'user_option' => $userAns,
                                        'is_correct' => $is_correct,
                                        'option_status' => $option_status,
                                    ]); 
                            }
                    }else{
                            $is_correct = '0';
                            $option_status = '0';
                            $data = AnswerOptions::create([
                                'user_id' => $user_id,
                                'ques_id' => $questionid,
                                'user_option' => null,
                                'is_correct' => $is_correct,
                                'option_status' => $option_status,
                            ]); 
                    }
            }

             // for true and false type questions. 
            if($question_type == '3'){
                if (isset($_POST[$questionid])) 
                {
                       if($correctOption == $_POST[$questionid]){
                            $is_correct = '1';
                            $option_status = '1';
                            $data = AnswerOptions::create([
                                'user_id' => $user_id,
                                'ques_id' => $questionid,
                                'user_option' => $_POST[$questionid],
                                'is_correct' => $is_correct,
                                'option_status' => $option_status,
                            ]);
                    }else{
                            $is_correct = '0';
                            $option_status = '1';
                            $data = AnswerOptions::create([
                                'user_id' => $user_id,
                                'ques_id' => $questionid,
                                'user_option' => $_POST[$questionid],
                                'is_correct' => $is_correct,
                                'option_status' => $option_status,
                            ]); 
                    }

                }else{
                    $is_correct = '0';
                            $option_status = '0';
                            $data = AnswerOptions::create([
                                'user_id' => $user_id,
                                'ques_id' => $questionid,
                                'user_option' => null,
                                'is_correct' => $is_correct,
                                'option_status' => $option_status,
                            ]); 
                }
            }
        }

        $totalQuestion = AnswerOptions::where(['user_id' => $user_id])->count();
        $curectAns     = AnswerOptions::where(['user_id' => $user_id])->where(['option_status' => '1'])->where(['is_correct' => '1'])->count();
        $wrongAns      = AnswerOptions::where(['user_id' => $user_id])->where(['option_status' => '1'])->where(['is_correct' => '0'])->count();
        $uncurectAns   = AnswerOptions::where(['user_id' => $user_id])->where(['option_status' => '0'])->count();
        $hun = '100';
        $score = $curectAns/$totalQuestion*$hun;
        $score = number_format($score);
        $url = url('/assessment/result/').$user_id; 
        //mail section 
        $data = "Hi ,". PHP_EOL .
                    "Student Name: " .$user_name . PHP_EOL .
                    "User Email: ".$user_email. PHP_EOL. 
                    "Total Question: " . $totalQuestion . PHP_EOL . 
                    "Correct Answer: " . $curectAns . PHP_EOL .
                    "Wrong Answer: " . $wrongAns . PHP_EOL .
                    "UnAttempted Question: " . $uncurectAns . PHP_EOL .
                    "User Score: " . $score . "%" . PHP_EOL .
                    "Please click here for result: " . $url . PHP_EOL .
                    "Thanks" ;

        $data_ema = 'goyalrockin@gmail.com';
       // $data_ema = 'info@syptus.com';
        $data_el = array( 'email' => $data_ema);
        
        Mail::raw( $data , function($message)  use ($data_el){                        
                $message->to($data_el['email'])->subject('Result');
        });

        $start_test_result = array(

                'user_id' => $user_id,
                'user_email' => $user_email,
                'name' => $user_name,
                'test_time' => $start_time,
                'test_score' => $score,
                'curectAns' => $curectAns,
                'wrongAns' => $wrongAns,
                'uncurectAns' => $uncurectAns

            );
        Testresult::insert($start_test_result);


        User::where('id','=',$user_id)->update(['test_status' => 1]);
        return view('user_section.thank_you')->with([
            'user' => Auth::user(),
            'action' => 'add',
            'page_title' => 'Add Integration',
            'menu' => [
            'tree' => 'integrations',
            'item' => 'new-integration'
            ]
            ]);
    }


    public function user_list ()
    {     
        $user_list =  User::where(['role_id' => '1'])->get();   
        return view('user_section.user_list')->with([
            'user' => Auth::user(),
            'action' => 'add',
            'user_list' => $user_list,
            'page_title' => 'Add Integration',
            'menu' => [
            'tree' => 'integrations',
            'item' => 'new-integration'
            ]
            ]);
    }


    public function add_user (Request $request)
    {

        if ($request->all())
        {

            $name = $_POST['name'];
            $email = $_POST['email'];
            $random = substr(md5(mt_rand()), 0, 7);
            $password = bcrypt($random);
            $role_id = '1';
            $test_status = '0';
            $addUser = User::create([
                    'name' => $name,
                    'email' => $email,
                    'password' => $password,
                    'role_id' => $role_id,
                    'test_status' => $test_status
            ]);

            $url = url('/login');
            $data = "Hi ,". PHP_EOL .
                        "Name: " .$name . PHP_EOL .
                        "Email: ".$email. PHP_EOL. 
                        "Password: " . $random . PHP_EOL .
                        "For Login Click here: " . $url . PHP_EOL .
                        "Thanks" ;
            $data_el = array( 'email' => $email);
            Mail::raw( $data , function($message)  use ($data_el){                        
                    $message->to($data_el['email'])->subject('Login Details');
            });
            return redirect(url('/user_list'));
        }

            return view('user_section.add_user')->with([
            'user' => Auth::user(),
            'action' => 'add',
            'page_title' => 'Add Integration',
            'menu' => [
            'tree' => 'integrations',
            'item' => 'new-integration'
            ]
            ]);
    }


    public function user_view ()
    {
        $user_id = $_GET['id'];
        $test = User::select('test_status')->where('id','=',$user_id)->first();
        $test_status = $test->test_status;
        $test_result = '';
        if($test_status == 1)
        {
           $test_result = Testresult::select('*')->where('user_id','=',$user_id)->first();
        }



        $userResult = User::where(['id' => $user_id])->first();
        return view('user_section.user_view')->with([
            'user' => Auth::user(),
            'action' => 'add',
            'userResult' => $userResult,
            'test_result' => $test_result,
            'page_title' => 'Add Integration',
            'menu' => [
            'tree' => 'integrations',
            'item' => 'new-integration'
            ]
            ]);
    }


    public function user_edit (Request $request) 
    {

        $user_id = $_GET['id'];
        $userEdit = User::where(['id' => $user_id])->first();
        return view('user_section.user_edit')->with([
            'user' => Auth::user(),
            'action' => 'add',
            'userEdit' => $userEdit,
            'page_title' => 'Add Integration',
            'menu' => [
            'tree' => 'integrations',
            'item' => 'new-integration'
            ]
            ]);

    }

    public function user_submit ()
    {
        $user_id = $_GET['id'];
        $name = $_POST['name'];
        $email = $_POST['email'];

        $save = User::where('id', $user_id)->update([
                'name' => $name,
                'email'=> $email,
        ]);
        return redirect(url('/user_list'));
    }
    public function user_delete()
    {   
        $user_id = $_GET['id'];
        $role_id = '2';
        $save = User::where('id', $user_id)->update([
                'role_id'=> $role_id,
        ]);
        return redirect(url('/user_list'));
    }



    public function getLogout() 
    {
             Auth::logout();
            return redirect(url('/'));
    }


    public function get404()
    {
         return view('404');
    }

    public function test_started()
    {

        return view('user_section.test_started')->with([
            'user' => Auth::user(),
            'action' => 'add',
            'page_title' => 'Add Integration'    
        ]);   
    }
}
