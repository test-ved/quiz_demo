<?php

namespace App\Http\Controllers;

// use App\Question;
// use App\QuestionsOption;
use App\Questionnaires;
use Illuminate\Http\Request;
use App\Topic;
use App\Type;
use Illuminate\Support\Facades\Input;
use Auth; 
use Mail;
use App\AnswerOptions;
use Session;
use Carbon\Carbon;
use Validator;
use Illuminate\Http\Response;
use Illuminate\Contracts\Routing\ResponseFactory;

class QuestionController extends Controller
{


    /**
     * Display a listing of Question.
     *
     * @return \Illuminate\Http\Response
     */


     public function __construct()
    {
        $this->middleware('auth');
    }


    public function index()
    {
        $questions = Questionnaires::all();

        return view('questions.index', compact('questions'));
    }

    /**
     * Show the form for creating new Question.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       

       $relations = Topic::select('id','title')->get()->toArray();
       $type = Type::select('id','type')->get()->toArray();
        return view('questions.create')->with([
        'relations' => $relations,
        'type' => $type
        ]);

    } 

     public function store(Request $request)
    {

        $all = $request->all();
        unset($all['_token']);





        $random_token = uniqid(rand(10,10000000),true);
          $random_token  = md5($random_token);
          $date = date('Y-m-d H:i:s');


         $A = $B  = $C = $D = $E  = $correct_option =  $direction_title = '' ;

        if(isset($all['A']))
        {
            $A = $all['A'];
        }
        if(isset($all['B']))
        {
            $B = $all['B'];
        }
        if(isset($all['C']))
        {
            $C = $all['C'];
        }
        if(isset($all['D']))
        {
            $D = $all['D'];
        }
        if(isset($all['E']))
        {
            $E = $all['E'];
        }
        if(isset($direction_title))
        {
            $direction_title = $all['direction_title'];
        }


        if($all['type_name'] == '2' || $all['type_name'] == '5') {
        
            if(isset($all['option_corre'])) {

                $correct_option = explode(' ',$all['option_corre']);
                $corr_ans = json_encode($correct_option);
                $corr_ans = base64_encode($corr_ans);

            }
        }

        
        $ques_ans = array(

            'A' => $A,
            'B' => $B,
            'C' => $C,
            'D' => $D,
            'E' => $E

            );
        $ques_ans = json_encode($ques_ans);
        $ques_ans = base64_encode($ques_ans);

        

        if($all['type_name'] == '1' || $all['type_name'] == '3' || $all['type_name'] == '4') {
        
            $corr_ans = $all['option_corre'];
        }


        $random_token = uniqid(rand(10,10000000),true);
        $random_token  = md5($random_token);
        $questionnaires = array(

                'question' => $all['ques_title'],
                'options' => $ques_ans,
                'is_correct' => $corr_ans,
                'question_status' => '0',
                'question_type' => $all['type_name'],
                'answer_explanation' => $all['ans_expl'],
                'direc_text' => $direction_title,
                'random_token' => $random_token


            );

        Questionnaires::insert($questionnaires);



    $questions = Questionnaires::all();
    return redirect(url('/questions'));

    }

    public function get_result($id){

        $user_id = $id;
        
        $totalQues = AnswerOptions::where(['user_id' => $user_id])->count();
        $curectAns = AnswerOptions::where(['user_id' => $user_id])->where(['is_correct' => '1'])->count();
        $wrongAns = AnswerOptions::where(['user_id' => $user_id])->where(['is_correct' => '0'])->count();
        $questionID = AnswerOptions::where(['user_id' => $user_id])->get();
        foreach ($questionID as $key)
        {
            $quesID[] = $key->ques_id;
        }
        $questionResult = Questionnaires::whereIn('id',$quesID)->get();
        $questionAnswer = AnswerOptions::whereIn('ques_id',$quesID)->get();

        foreach ($questionAnswer as $key) 
        {   
              $ques_id[] =  $key->ques_id;
              $userAns[] =  $key->user_option;   
        }
        $questionAnswer = array_combine($ques_id,$userAns);

        $hun = '100';
        $score = $curectAns/$totalQues*$hun;
        return view('questions.result')->with([
            'user' => Auth::user(),
            'action' => 'add',
            'totalQueston' => $totalQues,
            'currectAnswer' => $curectAns,
            'wrongAnswer' => $wrongAns,
            'questionResult' => $questionResult,
            'score' => $score,
            'questionAnswer' => $questionAnswer,
            'page_title' => 'Add Integration',
            'menu' => [
            'tree' => 'integrations',
            'item' => 'new-integration'
            ]
            ]);
    }

}
