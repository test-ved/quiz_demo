<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Questionnaires extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id','question','options','is_correct','question_status','direc_text'
    ];

    /**
     * Name of the Sysapp table
     *
     * @var string
     */
    protected $table = 'questionnaires';

    public $timestamps = false;
    
    
}
