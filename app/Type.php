<?php
namespace App;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Type
 *
 * @package App
 * @property string $title
*/
class Type extends Model
{
    use SoftDeletes;

    protected $table = 'types';

    protected $fillable = ['type'];


}
