<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AnswerOptions extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id','ques_id','user_option','is_correct','option_status'
    ];

    /**
     * Name of the Sysapp table
     *
     * @var string
     */
    protected $table = 'answer_options';

    public $timestamps = false;
    
    
}
