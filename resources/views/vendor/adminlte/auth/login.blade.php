@extends('adminlte::layouts.auth')

@section('htmlheader_title')
    Log in
@endsection

@section('content')
<body class="hold-transition login-page">

 @if (Auth::guest())

    <div id="app" v-cloak>
        <div class="login-box">
            <div class="login-logo" style="color: #fff;">
               <!--  <a href="{{ url('/home') }}"> -->Quiz <b>Demo</b><!-- </a> -->
            </div><!-- /.login-logo -->

        @if (count($errors) > 0)
            <div class="alert alert-danger">
                <strong>Whoops!</strong> {{ trans('adminlte_lang::message.someproblems') }}<br><br>
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        <div class="login-box-body">
        <p class="login-box-msg"> {{ trans('adminlte_lang::message.siginsession') }} </p>

        <login-form name="{{ config('auth.providers.users.field','email') }}"
                    domain="{{ config('auth.defaults.domain','') }}"></login-form>

<!--         @include('adminlte::auth.partials.social_login') 

        <a href="{{ url('/password/reset') }}">{{ trans('adminlte_lang::message.forgotpassword') }}</a><br>
        <a href="{{ url('/register') }}" class="text-center">{{ trans('adminlte_lang::message.registermember') }}</a> -->

    </div>

    </div>
    </div>
    @include('adminlte::layouts.partials.scripts_auth')

    <script>
        $(function () {
            $('input').iCheck({
                checkboxClass: 'icheckbox_square-blue',
                radioClass: 'iradio_square-blue',
                increaseArea: '20%' // optional
            });
        });
    </script>

@else

 <div id="app" >
        <div class="login-box">
            <div class="login-logo">
                <a style="color: #fff;" href="{{ url('/home') }}"><b>Quiz</b>Demo</a>
            </div><!-- /.login-logo -->

        @if (count($errors) > 0)
            <div class="alert alert-danger">
                <strong>Whoops!</strong> {{ trans('adminlte_lang::message.someproblems') }}<br><br>
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        <div class="login-box-body" style="text-align: center;">
        <p class="login-box-msg"> {{ trans('adminlte_lang::message.siginsession') }} </p>
        <a style="margin-left: auto; margin-right: auto; " href="{{ url('/home') }}"><button class="btn btn-primary">Go to Dashboard</button></a>
        <a style="margin-left: auto; margin-right: auto; " href="{{ url('logout') }}"><button class="btn btn-primary">Sign Out</button></a>

    </div>

    </div>
</div>
  <script>
        $(function () {
            $('input').iCheck({
                checkboxClass: 'icheckbox_square-blue',
                radioClass: 'iradio_square-blue',
                increaseArea: '20%' // optional
            });
        });
    </script>
 @endif


</body>

@endsection
