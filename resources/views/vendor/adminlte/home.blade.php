@extends('adminlte::layouts.app')
@section('htmlheader_title')
{{ trans('adminlte_lang::message.home') }}
@endsection
@section('main-content')
<section class="content-header">
   <h1>
      Dashboard
   </h1>
</section>
<section class="content">
   <div class="row">
      <div class="col-lg-3 col-xs-6">
         <div class="small-box bg-yellow">
            <div class="inner">
               <p><?php echo $user_count; ?> Users</p>
            </div>
            <a href="{{url('/user_list')}}" class="small-box-footer">More <i class="fa fa-arrow-circle-right"></i></a>
         </div>
      </div>
      <div class="col-lg-3 col-xs-6">
         <div class="small-box bg-red">
            <div class="inner">
               <p><?php echo $question_count; ?> Questions</p>
            </div>
            <a href="{{url('questions')}}" class="small-box-footer">More <i class="fa fa-arrow-circle-right"></i></a>
         </div>
      </div>
   </div>
   <div class='row'>
      <div class="col-md-4">
         <div class="box box-primary">
            <div class="box-header with-border">
               <h3 class="box-title">{{ @$test_count }} Users Given Test</h3>
               <div class="box-tools pull-right">
                  <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                  </button>
                  <!--<button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>-->
               </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body" style="display: block;">
               <ul class="products-list product-list-in-box">
                  <li class="item">
                  <?php $i = 1; ?>
                     <table style="font-family: arial, sans-serif;border-collapse: collapse;width: 100%;">
                           <tr>
                              <th style="border: 1px solid #dddddd;text-align: left;padding: 8px;">Sr. No. </th>
                              <th style="border: 1px solid #dddddd;text-align: left;padding: 8px;">User Name</th>
                              <th style="border: 1px solid #dddddd;text-align: left;padding: 8px;">Test Score</th>
                           </tr>
                        @foreach($test_details as $val)
                           <tr>
                              <td style="border: 1px solid #dddddd;text-align: left;padding: 8px;">
                                 <?php echo $i; ?>
                              </td>
                             <?php $i++; ?>
                              <td style="border: 1px solid #dddddd;text-align: left;padding: 8px;">
                                 {{$val->name}}
                              </td>
                              <td style="border: 1px solid #dddddd;text-align: left;padding: 8px;">
                                 {{$val->test_score}}%
                              </td>
                           </tr>
                        @endforeach
                     </table>
                  </li>
               </ul>
            </div>
         </div>
      </div>
   </div>
</section>
@endsection