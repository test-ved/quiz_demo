@extends('adminlte::layouts.app')




@section('main-content')

    <link rel="stylesheet" href="{{ asset('css/datatable.css')}}">
    <link rel="stylesheet" href="{{ asset('css/dt.css')}}">
    <script src="{{ asset('js/datatable.js')}}" type="text/javascript"></script>
    <script src="{{ asset('js/data.js')}}" type="text/javascript"></script>

    <h3 class="page-title">Questions</h3>

    <p>
        <a href="{{ url('questions/create') }}" class="btn btn-success">Add New</a>
    </p>

    <div class="panel panel-default">
        <div class="panel-heading">
            List
        </div>

        <div class="panel-body">
            <table class="table table-bordered table-striped dt-select display" id="datatable">
                <thead>
                    <tr>
                        <th style="text-align:center;">S.No</th>
                        <th>Question</th>
<!--                         <th style="text-align:center;">Actions</th> -->
                    </tr>
                </thead>
                
                <tbody>
                    	<?php $i =1; ?>
                    @if (count($questions) > 0)
                        @foreach ($questions as $question)

                            <tr data-entry-id="{{ $question->id }}">
                                <td> <?php echo $i;?> </td>
                                <td>{!! $question->question !!}</td>
<!--                                 <td style="text-align:center;">

                                    <a href={{ url('/assessment/user/user_view')}}><i class="fa fa-eye" style="color: #367fa9;" aria-hidden="true"></i> 
                                    </a>

                                    <a href={{ url('/assessment/user/user_edit')}}> <i class="fa fa-pencil-square" style="color: #367fa9;" aria-hidden="true"></i> 
                                    </a>

                                    <a href={{ url('/assessment/user/user_delete')}}><i class="fa fa-trash" style="color: #367fa9;" aria-hidden="true"></i>
                                    </a>

                                </td> -->
                            </tr>
                            <?php $i++; ?>
                        @endforeach
                    @else
                        <tr>
                            <td colspan="7">No Entries In Table</td>
                        </tr>
                    @endif
                </tbody>
            </table>
        </div>
    </div>


@endsection