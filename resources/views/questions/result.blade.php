@extends('adminlte::layouts.app')

@section('htmlheader_title')
    {{ trans('adminlte_lang::message.home') }}
@endsection

@section('main-content')

    <div class="container-fluid">
        <div class="row">
            <div class="col-md-11 content-boxes col-md-offset-0" style="margin-left: 4%;">
                <div class="panel panel-default">
                    <div class="panel-heading" style="background-color: #367fa9; color: white;">User Result</div>
                     <div class="panel-body">
                        <div class="form form-horizontal">
                           <div class="div-test-initiator" id="divInitiator">
                                 <div class="mx-none" id="divStartTestInstruction">
                                    <div class="div-test-instruction">
                                            <ul class="ul-test-instruction">
                                                <li>Total number of questions : <b><?php echo $totalQueston; ?></b></li>
                                                <li>Time alloted : <b>30</b> minutes.</li>
                                                <li>Currect Answer : <b><?php echo $currectAnswer; ?></b></li>
                                                <li>Wrong Answer : <b><?php echo $wrongAnswer; ?></b>
                                                </li>
                                                <li>Score : <b><?php echo number_format($score); ?>%</b></li>
                                            </ul>
                                    </div>
                                 </div>
                            </div>
                        </div>
                        <hr>
                        <?php
                            $i = sizeof($questionResult);
                            for ($j=0; $j<$i; $j++)
                            { 
                                $data =  json_decode(base64_decode($questionResult[$j]['options']));
                                $data =   (array) $data;
                                ?>
                            <?php 
                            if ($questionResult[$j]['question_type'] == '1' || $questionResult[$j]['question_type'] == '4'){
                                ?>
                                <fieldset id="">
                                    <table style="width: 100%;">
                                        <tbody>
                                            <tr>
                                            &nbsp;
                                                <td>
                                                 Q <?php echo $questionResult[$j]['id']; ?> - <?php echo $questionResult[$j]['question']; ?>
                                                 <?php 

                                                 $objcurrectAns = $questionResult[$j]['is_correct'];
                                                 $objuserAns = $questionAnswer[$questionResult[$j]['id']];

                                                    if ($objcurrectAns == $objuserAns )
                                                     {
                                                       ?>
                                                       <i style="float: right; font-size: 31px; color: green;" class="fa fa-check-circle" aria-hidden="true" title="Correct Answer"></i>
                                                       <?php
                                                     }else if ($objuserAns != '') 
                                                     {
                                                        ?>
                                                            <i style="float: right; font-size: 31px; color: red;" class="fa fa-times-circle" aria-hidden="true" title="Wrong Answer"></i>
                                                        <?php
                                                     }else {
                                                        ?>
                                                            <i style="float: right; font-size: 25px; color: red;"class="fa fa-window-minimize" aria-hidden="true" title="Not Attempted"></i>
                                                        <?php
                                                     }
                                                 ?>
                                                 </br>&nbsp;
                                                 <div>
                                                        A:
                                                        <label for="designercheckbox"><?php echo $data['A']; ?></label>
                                                 </div>
                                                 <div>
                                                        B:
                                                        <label for="backendcheckbox"><?php echo $data['B']; ?></label>
                                                </div>
                                                 <div>
                                                        C:
                                                        <label for="designercheckbox"><?php echo $data['C']; ?></label>
                                                </div>
                                                 <div>
                                                        D:
                                                        <label for="designercheckbox"><?php echo $data['D']; ?></label>
                                                </div>
                                                <hr>
                                                <div>
                                                        <strong>Correct Answer:</strong>
                                                        <label for="designercheckbox"><?php echo $objcurrectAns; ?></label>
                                                </div>
                                                <div>
                                                        <strong>User Answer:</strong>
                                                        <label for="designercheckbox">
                                                        <?php 
                                                            if (isset($objuserAns))
                                                            {
                                                                echo $objuserAns;
                                                            }else{
                                                                echo "Not Attempted.";
                                                            }

                                                        ?>
                                                        </label>
                                                </div>
                                                <div>
                                                        <strong>Explanation:</strong>
                                                        <label for="designercheckbox"><?php echo $questionResult[$j]['answer_explanation']; ?></label>
                                                </div>
                                                 <hr>
                                                </td>
                                            </tr>

                                        </tbody>
                                    </table>
                             </fieldset>
                                <?php
                                }
                                 if($questionResult[$j]['question_type'] == '2'){
                                   
                                    ?>
                                       <fieldset id="">
                                    <table>
                                        <tbody>
                                            <tr>
                                            &nbsp;
                                                <td>
                                                 Q <?php echo $questionResult[$j]['id'];; ?> - <?php echo $questionResult[$j]['question']; ?>

                                                 <?php 
                                                     $currectAns = base64_decode($questionResult[$j]['is_correct']);
                                                     $currectAns = implode(",", json_decode($currectAns));

                                                     $userAns = base64_decode($questionAnswer[$questionResult[$j]['id']]); 
                                                     $userAns = implode(",", json_decode($userAns));

                                                     if ($currectAns == $userAns )
                                                     {
                                                       ?>
                                                       <i style="float: right; font-size: 31px; color: green;" class="fa fa-check-circle" aria-hidden="true" title="Correct Answer"></i>
                                                       <?php
                                                     }else{
                                                        ?>
                                                        <i style="float: right; font-size: 31px; color: red;" class="fa fa-times-circle" aria-hidden="true" title="Wrong Answer"></i>
                                                        <?php
                                                     }

                                                 ?>

                                                 </br>&nbsp;
                                                 <div>
                                                        A:
                                                        <label for="designercheckbox"><?php echo $data['A']; ?></label>
                                                 </div>
                                                 <div>
                                                        B:
                                                        <label for="backendcheckbox"><?php echo $data['B']; ?></label>
                                                </div>
                                                 <div>
                                                        C:
                                                        <label for="designercheckbox"><?php echo $data['C']; ?></label>
                                                </div>
                                                 <div>
                                                        D:
                                                        <label for="designercheckbox"><?php echo $data['D']; ?></label>
                                                </div>
                                                
                                                <?php 
                                                if (isset($data['E'])){
                                                    ?>
                                                    <div>
                                                        E:
                                                        <label for="designercheckbox"><?php echo $data['E']; ?></label>
                                                    </div>
                                                    <?php
                                                }else{

                                                }
                                                ?>
                                                <hr>
                                                <div>
                                                        <strong>Correct Answer:</strong>
                                                        <label for="designercheckbox">
                                                        <?php 
                                                            print_r($currectAns);
                                                        ?>
                                                        </label>
                                                </div>
                                                <div>
                                                        <strong>User Answer:</strong>
                                                        <label for="designercheckbox">
                                                             <?php 
                                                               print_r($userAns);
                                                            ?>
                                                        </label>
                                                </div>
                                                <div>
                                                        <strong>Explanation:</strong>
                                                        <label for="designercheckbox"><?php echo $questionResult[$j]['answer_explanation']; ?></label>
                                                </div>
                                                 <hr>
                                                </td>
                                            </tr>

                                        </tbody>
                                    </table>
                             </fieldset>  
                                    <?php
                                    }
                             if ($questionResult[$j]['question_type'] == '3'){

                                ?>
                                        <fieldset id="">
                                    <table>
                                        <tbody>
                                            <tr>
                                            &nbsp;
                                                <td>
                                                 Q <?php echo $questionResult[$j]['id']; ?> - <?php echo $questionResult[$j]['question']; ?></br>&nbsp;
                                                 <div>
                                                        True:
                                                        <label for="designercheckbox"><?php echo $data['A']; ?></label>
                                                 </div>
                                                 <div>
                                                        False:
                                                        <label for="backendcheckbox"><?php echo $data['B']; ?></label>
                                                </div>
                                                <hr>
                                                <div>
                                                        <strong>Correct Answer:</strong>
                                                        <label for="designercheckbox"><?php echo $questionResult[$j]['is_correct']; ?></label>
                                                </div>
                                                <div>
                                                        <strong>User Answer:</strong>
                                                        <label for="designercheckbox">
                                                        <?php 
                                                            echo $questionAnswer[$questionResult[$j]['id']]; 
                                                        ?>
                                                        </label>
                                                </div>
                                                <div>
                                                        <strong>Explanation:</strong>
                                                        <label for="designercheckbox"><?php echo $questionResult[$j]['answer_explanation']; ?></label>
                                                </div>
                                                 <hr>
                                                </td>
                                            </tr>

                                        </tbody>
                                    </table>
                             </fieldset> 
                                <?php
                                        }

                            ?>
                                <?php
                            }
                           ?>
                           <?php
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection