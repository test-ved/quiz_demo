@extends('adminlte::layouts.app')
@section('main-content')
<!-- <style type="text/css">
    .select2-container
    {
    width: 100% !important;
    }
</style> -->
<div class="container-fluid">
    <div class="row">
        <div class="col-md-11 content-boxes col-md-offset-0" style="margin-left: 4%;">
            <div class="panel panel-default">
                <div class="panel-heading" style="background-color: #367fa9; color: white;">Add Question</div>
                <div class="panel-body">
                    <div class="form form-horizontal">
                        {!! Form::open(['method' => 'POST', 'url'=>'questions/store']) !!}
                        <?php $size_relations = sizeof($relations);?>
                        <?php $size_type = sizeof($type);?>

                        <div class="form-group" id='topi' style='display:block'>
                            <label for="name" class="col-lg-3 control-label required">Topic</label>
                            <div class="col-lg-9">
                                <select id="topic_name" class="form-control" name="topic_name" required='required' >
                                    <?php for($i =0; $i < $size_relations; $i++) { ?>
                                    <option value=<?php echo $relations[$i]['id']; ?>><?php echo $relations[$i]['title']; ?></option>
                                    <?php } ?>    
                                </select>
                            </div>
                        </div>

                        <div class="form-group" id='typw' style='display:block'>
                            <label for="name" class="col-lg-3 control-label required">Type</label>
                            <div class="col-lg-9">
                                <select id="type_name" class="form-control" name="type_name" required='required' >
                                    <?php for($i =0; $i < $size_type; $i++) { ?>
                                    <option value=<?php echo $type[$i]['id']; ?>><?php echo $type[$i]['type']; ?></option>
                                    <?php } ?>    
                                </select>
                            </div>
                        </div>

                        <div class="form-group" id='direction_text' style='display:block'>
                            <label for="name" class="col-lg-3 control-label required">Directions</label>
                            <div class="col-lg-9">
                                <textarea name='direction_title'  id='direction_title' placeholder='Enter Direction' class='form-control section_title' cols='30' rows='5' ></textarea>
                            </div>
                        </div>
                        <input type="hidden" name="dire_ques" id="dire_ques" value="1">

                        <div class="form-group" id='question_text' style='display:block'>
                            <label for="name" class="col-lg-3 control-label required">Question</label>
                            <div class="col-lg-9">
                                <textarea name='ques_title'  id='ques_title' placeholder='Enter Question' class='form-control section_title' cols='30' rows='5' ></textarea>
                            </div>
                        </div>

                        <div class='form-group' id='opt_a' style='display:block'>
                            <label for='option' class='col-lg-3 control-label'>Option A </label>
                            <div class='col-lg-9'>
                                <input name='A'  id='option_A' class='inout_option' placeholder='Enter Option A' class='form-control option'>
                            </div>
                        </div>

                        <div class='form-group' id='opt_b' style='display:block'>
                            <label for='option' class='col-lg-3 control-label'>Option B </label>
                            <div class='col-lg-9'>
                                <input name='B'  id='option_B' class='inout_option' placeholder='Enter Option B' class='form-control option'>
                            </div>
                        </div>

                        <div class='form-group' id='opt_c' style='display:block'>
                            <label for='option' class='col-lg-3 control-label'>Option C </label>
                            <div class='col-lg-9'>
                                <input name='C'  id='option_C' class='inout_option' placeholder='Enter Option C' class='form-control option'>
                            </div>
                        </div>

                        <div class='form-group' id='opt_d' style='display:block'>
                            <label for='option' class='col-lg-3 control-label'>Option D </label>
                            <div class='col-lg-9'>
                                <input name='D'  id='option_D' class='inout_option'  placeholder='Enter Option D' class='form-control option'>
                            </div>
                        </div>

                        <div class='form-group' id='opt_e' style='display:block'>
                            <label for='option' class='col-lg-3 control-label'>Option E </label>
                            <div class='col-lg-9'>
                                <input name='E'  id='option_E' class='inout_option' placeholder='Enter Option E' class='form-control option'>
                            </div>
                        </div>

                        <div class='form-group' id='opt_corr' style='display:block'>
                            <label for='option' class='col-lg-3 control-label'>Correct Option </label>
                            <div class='col-lg-9'>
<!-- 
                                <select multiple="multiple"  name="option_corection" class="js_example" class="form-control">
                                      <option value="A">Option A</option>
                                      <option value="B">Option B</option>
                                      <option value="C">Option C</option>
                                      <option value="D">Option D</option>
                                      <option value="E">Option E</option>
                                </select> -->

                                <input name='option_corre'  id='option_corre' placeholder='Correct Option' class='form-control option'>

                            </div>
                        </div>

                        <div class="form-group" id='ans_exp' style='display:block'>
                            <label for="name" class="col-lg-3 control-label required">Answer Explanation</label>
                            <div class="col-lg-9">
                                <textarea name='ans_expl'  id='ans_expl' placeholder='Answer Explanation' class='form-control section_title' cols='30' rows='5' ></textarea>
                            </div>
                        </div>



                            {!! Form::submit('Save', ['class' => 'btn btn-info']) !!}



                        {!! Form::close() !!}
                    </div>
                </div>
                <!-- <span class="mandatory">Fields marked with an asterisk (*) are mandatory</span> -->
            </div>
        </div>
    </div>
</div>
</div>


<script type="text/javascript" src="{{ asset('/js/tinymce/tinymce.min.js') }}"></script>
<script type="text/javascript">
  tinymce.init({
    selector : "textarea",
    height: 100,
    plugins : ["advlist autolink lists link image charmap print preview anchor", "searchreplace visualblocks code fullscreen", "insertdatetime media table contextmenu paste"],
    toolbar : "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image"
  }); 

  tinymce.init({
    selector : "input.inout_option",
    plugins : ["advlist autolink lists link image charmap print preview anchor", "searchreplace visualblocks code fullscreen", "insertdatetime media table contextmenu paste"],
    toolbar : "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image"
  }); 

</script>

<!--     <script src="{{ asset('/js/add_ques.js') }}"></script> -->
<!-- <link rel="stylesheet" type="text/css" href="{{ asset('css/select2.min.css') }}">
<script src="{{ asset('/js/select2.min.js') }}"></script>

  

<script type="text/javascript">
    $(".js_example").select2();
</script> -->



@endsection