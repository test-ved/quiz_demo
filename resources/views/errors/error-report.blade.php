@extends('app')

@section('content')

@section('pageType', 'error')

<div class="404page">
	<div class="header"></div>
	<div class="container">
		<div class="col-md-6">
			<p>Client not setup in PowerBI</p>
			<p>Don't worry you will be back on track in no time!</p>
			<h2>The page you were looking for doesn't exist. Please try something else</h2>
			<a class="dashboard-btn" href="{{url('dashboard')}}" >Go to Dashboard</a>
		</div>
		<div class="col-md-6">
			<img src="{{url('/assets/images/right-img.png')}}">
		</div>
	</div>
</div>

@endsection




