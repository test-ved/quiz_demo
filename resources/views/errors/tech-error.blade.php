

<div class="404page">
	<div class="header"></div>
	<div class="container">
		<div class="col-md-12">
			<!--<p>Technical Error!</p>-->
			<p style="font-size: 32px;">System error encountered.</p>
			<h2>Admin has been notified and we will rectify this ASAP.</h2>
			<a class="dashboard-btn" href="{{url('/')}}" >Return Back</a>
		</div>
		
	</div>
</div>