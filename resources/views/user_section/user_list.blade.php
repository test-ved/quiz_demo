@extends('adminlte::layouts.app')



@section('main-content')

    <h3 class="page-title">Users</h3>
   
    <p>
        <a href="{{url('/add_user')}}" class="btn btn-success">Add User</a>
    </p>

    <div class="panel panel-default">
        <div class="panel-heading">
            List
        </div>

        <div class="panel-body">
            <table class="table table-bordered table-striped dt-select">
                <thead>
                    <tr>
                        <th style="text-align:center;">ID</th>
                        <th style="text-align:center;">Name</th>
                        <th style="text-align:center;">Email</th>
                        <th style="text-align:center;">Test</th>
                        <th style="text-align:center;">Actions</th>
                    </tr>
                </thead>
                    
                <tbody>
                <?php 
                    foreach ($user_list as $key) 
                    {
                        $id = $key->id;
                    ?>
                        <tr>
                                <td style="text-align:center;"> <?php echo $key->id; ?> </td>
                                <td style="text-align:center;"> <?php echo $key->name; ?> </td>
                                <td style="text-align:center;"> <?php echo $key->email; ?> </td>
                                <?php if($key->test_status == 1) {   ?>
                                <td style="text-align:center;"> Yes</td>
                                <?php } ?>
                                <?php if($key->test_status == 0) {   ?>
                                <td style="text-align:center;"> No </td>
                                <?php } ?>
                                <td style="text-align:center;">
                                <a style="color: #fff;" href={{ url('/assessment/user/user_view')."?id=".$id}}><button class="btn btn-info">View</button>
                                </a> 
<!--                                 <a href={{ url('/assessment/user/user_edit')."?id=".$id}}><button class="btn btn-primary">Edit</button></a> -->

<!--                                 <a href={{ url('/assessment/user/user_delete')."?id=".$id}}><button class="btn btn-danger">Delete</button></a> -->
                                </td>
                        </tr>
                        <?php
                    }
                ?>
                </tbody>
            </table>
        </div>
    </div>

@endsection