@extends('adminlte::layouts.app')
@section('main-content')
 
<div class="container-fluid">
        <div class="row">
            <div class="col-md-11 content-boxes col-md-offset-0" style="margin-left: 4%; margin-top: 5%">
                <div class="panel panel-default">
                    <div class="panel-heading" style="background-color: #367fa9; color: white;">User Details
                    </div>
                    <div class="panel-body">
                        <div class="form form-horizontal">
                           <div class="div-test-initiator" id="divInitiator">
                                 <div class="mx-none" id="divStartTestInstruction">
                                    <div class="div-test-instruction">
                                            <ul class="ul-test-instruction">
                                                <li>Name : &nbsp; <?php echo $userResult->name; ?></li>
                                                <li>Email : &nbsp; <?php echo $userResult->email; ?> </li>
                                                <li>Test Status : &nbsp; <?php if($userResult->test_status == 1) {echo "Yes";} else {echo "No";} ?>
                                                <?php if($userResult->test_status == 1){?>
                                                <li>Nu of Questions : &nbsp; 15</li>
                                                <li>Correct Answer : &nbsp; <?php echo $test_result->curectAns;  ?></li>
                                                <li>Wrong Answer : &nbsp; <?php echo $test_result->wrongAns;  ?></li>
                                                <li>UnAttempted Questions : &nbsp; <?php echo $test_result->uncurectAns;  ?></li>
                                                <li>User Score : &nbsp; <?php echo $test_result->test_score; ?>%</li>



                                                <?php } ?>
                                                
                                            </ul>
                                    </div>
                                 </div>
                            </div>
                        </div>
                        <!-- <span class="mandatory">Fields marked with an asterisk (*) are mandatory</span> -->
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
