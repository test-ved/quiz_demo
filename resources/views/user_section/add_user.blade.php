@extends('adminlte::layouts.app')
@section('main-content')
<!-- <style type="text/css">
    .select2-container
    {
    width: 100% !important;
    }
</style> -->
<div class="container-fluid">
    <div class="row">
        <div class="col-md-11 content-boxes col-md-offset-0" style="margin-left: 4%;">
            <div class="panel panel-default">
                <div class="panel-heading" style="background-color: #367fa9; color: white;">Add User</div>
                <div class="panel-body">
                    <div class="form form-horizontal">

                       <form name="frm-add-integration" method="post" action="{{url('/add_user')}}">
                        {{csrf_field()}}
                            <div class="form-group" id='question_text' style='display:block'>
                                <label for="name" class="col-lg-3 control-label required">Name</label>
                                <div class="col-lg-9">
                                    <input name='name'  id='name' placeholder='Enter Name' class='form-control section_title' cols='30' rows='5' >
                                </div>
                            </div>
                            
                            <div class="form-group" id='question_text' style='display:block'>
                                <label for="email" class="col-lg-3 control-label required">Email</label>
                                <div class="col-lg-9">
                                    <input name='email'  id='email' placeholder='Enter Email' class='form-control section_title' cols='30' rows='5' >
                                </div>
                            </div>

                        <button style="float: right; margin-top:20px;" type="submit"  class="btn btn-primary submit">Submit</button>
                    
                        </form>
                    </div>
                </div>
                <!-- <span class="mandatory">Fields marked with an asterisk (*) are mandatory</span> -->
            </div>
        </div>
    </div>
</div>
</div>
@endsection