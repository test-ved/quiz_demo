<html lang="en">
@section('htmlheader')
    @include('adminlte::layouts.partials.htmlheader')
@show
<head>

    <script type='text/javascript'>
       /*  $(document).keydown(function(e){
         e.preventDefault();
        });  


        $(function() {
            $(this).bind("contextmenu", function(e) {
                e.preventDefault();
            });
        });  */



    </script>

     <script language ="javascript" >
        var tim;
        
        var min = 30;
        var sec = 00;
        var f = new Date();
        function f1() {
            timelimit = maxtimelimit;
            clearInterval(myVar);
           // startTimer();
            timelimit_new = maxtimelimit_new;
            clearInterval(myVar_new);
            //startTimer_new();
            document.getElementById("starttime").innerHTML = "Test Started At:" + f.getHours() + ":" + f.getMinutes();
            document.getElementById("start_time").value =  f.getHours() + ":" + f.getMinutes();
        }
       
    </script>
<style type="text/css">
  body {
overflow-y: scroll;
}

</style>

</head>
<body class="skin-blue sidebar-mini" onload="f1()">
<div class="container-fluid">
        <div class="row">
            <div class="col-md-11 content-boxes col-md-offset-0" style="margin-left: 4%; margin-top: 5%">
                <div class="panel panel-default">
                    <div class="panel-heading" style="background-color: #367fa9; color: white;">Test Dashboard
                        <div id="starttime" style="float: right;"></div>
                    </div>
                    <div class="panel-body" style="padding: 10px;">
                            <div>
                              <table width="100%" align="center">
                                    <strong>
                                    <div id="endtime"></div><br />
                                    <div id="timeleft" style="float: left;"></div>
                                     <div id="timeleft_new" style="float: right;"></div>

                                    </strong>
                              </table>
                              <br />
                            </div>
                    <hr>
                     <form name="frm-add-integration" method="post" id="message_detail" style="padding: 20px;" onsubmit="return confirm('Are you sure you want to submit?');"  action="{{url('/assessment/user/test_submit')}}">
                     {{csrf_field()}}
                     <?php 
                      foreach ($questions as $key) 
                      {
                        $questionID[] = $key['id']; 
                      }

                      $ID = implode(",", $questionID);
					  $i = sizeof($questions);
                     ?>   
					 <?php 
                            //$i = sizeof($questions);
                            for ($z=1; $z<=$i; $z++) 
                            {?> 
								<button class="linkspan" type="button" 
								id="<?php echo "currentbtn_".$z;?>" 
								style="margin: 5px; background: #ED4600" 
								>
								<span style="color: #FFFFFF" class="ng-binding ng-scope">
								<?php echo $z;?></span>
								</button>
							<?php }?>
				Total Viewed
				<input type="text" name="viewedtotal" value="" id="viewedtotal" />
				Total Answered 
				<input type="text" name="answeredtotal" value="" id="checkedanswer" />
				</br>
				Not Visited 
				<input type="text" name="notvisited" value="<?php echo $i;?>" id="notvisited" />
                     <input type="hidden" name="start_time" id="start_time"  value="">

                <input type="hidden" name="question_id" value="<?php echo $ID; ?>" id="question_id" />
                        <?php 
                            //$i = sizeof($questions);
                            for ($j=0; $j<$i; $j++) 
                            { 
                                $data =  json_decode(base64_decode($questions[$j]['options']));
                                $data =   (array) $data;
                              ?>   
                              <?php 

/**************************************** Question Type 1 ***************************************************************************/


                                if ($questions[$j]['question_type'] == '1'){
                                    ?>

                     <fieldset id="fieldset_<?php echo $j;?>">
                                    <table style="width:100%">
                                        <tbody style="width:100%">
                                            <tr>
                                            &nbsp;
                                                <!--leave it blank-->
                                                <td>
                                               
                                                     &nbsp;
                                          <div class="row" style="width:100%;margin-left:0px;margin-right:0px">
                                              <div class="col-sm-1" style="width:4.3%;padding-right:0px">
                                             Q <?php echo $j+1;?> 
                                              </div>
                                              <div class="col-sm-11" style=" white-space:normal;padding-left:0px;padding-right:0px;">
                                                <?php echo $questions[$j]['question']; ?> </br>&nbsp;
                                               </div>
                                              </div>
                                                
                                                 <div class="option" style="padding-left: 15px;">
                                                        <input type="radio" name="<?php echo $questions[$j]['id'] ?>" value="A" id="A" />
                                                        <label for="designercheckbox"><?php echo $data['A']; ?></label>
                                                 </div>
                                                 <div class="option" style="padding-left: 15px;">
                                                        <input type="radio" name="<?php echo $questions[$j]['id'] ?>" value="B" id="B" />
                                                        <label for="backendcheckbox"><?php echo $data['B']; ?></label>
                                                </div>
                                                 <div class="option" style="padding-left: 15px;">
                                                        <input type="radio" name="<?php echo $questions[$j]['id'] ?>" value="C" id="C" />
                                                        <label for="designercheckbox"><?php echo $data['C']; ?></label>
                                                </div>
                                                 <div class="option" style="padding-left: 15px;">
                                                        <input type="radio" name="<?php echo $questions[$j]['id'] ?>" value="D" id="D" />
                                                        <label for="D"><?php echo $data['D']; ?></label>
                                                </div>
                                                <?php

                                                if ($data['E'] != "") {
                                                    
                                                    ?>

                                                    <div class="option" style="padding-left: 15px;">
                                                        <input type="radio" name="<?php echo $questions[$j]['id'] ?>" value="E" id="E" />
                                                        <label for="E"><?php echo $data['E']; ?></label>
                                                    </div>
                                                    <?php
                                                }
                                                else
                                                {

                                                }
                                                ?>
                                             
                                                </td>
                                            </tr>

                                        </tbody>
                                    </table>
                                                <button style="float: right;" type="button"  class="btn btn-primary text">Next</button>
                                        <button style="float: right; margin-top:20px;" type="submit"  class="btn btn-primary submit">Complete Test</button>
                                </fieldset>
                                    <?php
                                }

/**************************************** Question Type 2 ***************************************************************************/




                                if($questions[$j]['question_type'] == '2'){
                                    ?>
                             <fieldset id="fieldset_<?php echo $j;?>">
                                    <table style="width:100%">
                                        <tbody style="width:100%">
                                            <tr>
                                            &nbsp;
                                                <!--leave it blank-->
                                                <td>
                                          <div class="row" style="width:100%;margin-left:0px;margin-right:0px">
                                              <div class="col-sm-1" style="width:4.3%;padding-right:0px">
                                             Q <?php echo $j+1;?> 
                                              </div>
                                              <div class="col-sm-11" style=" white-space:normal;padding-left:0px;padding-right:0px;">
                                                &nbsp; <?php echo $questions[$j]['question']; ?> </br>&nbsp;
                                                </div>
                                            </div>
                                                <input type="hidden" name="multi_type" value="<?php echo $questions[$j]['question_type']; ?>" />

                                                <input type="hidden" name="multi_question_id" value="<?php echo $questions[$j]['id']; ?>" />                                              

                                                 <div class="option" style="padding-left: 15px;">
                                                        <input type="checkbox" name="<?php echo $questions[$j]['id'] ?>[]" value="A" id="A" />
                                                        <label for="designercheckbox"><?php echo $data['A']; ?></label>
                                                 </div>
                                                 <div class="option" style="padding-left: 15px;">
                                                        <input type="checkbox" name="<?php echo $questions[$j]['id'] ?>[]" value="B" id="B" />
                                                        <label for="backendcheckbox"><?php echo $data['B']; ?></label>
                                                </div>
                                                 <div class="option" style="padding-left: 15px;">
                                                        <input type="checkbox" name="<?php echo $questions[$j]['id'] ?>[]" value="C" id="C" />
                                                        <label for="designercheckbox"><?php echo $data['C']; ?></label>
                                                </div>
                                                 <div class="option" style="padding-left: 15px;">
                                                        <input type="checkbox" name="<?php echo $questions[$j]['id'] ?>[]" value="D" id="D" />
                                                        <label for="D"><?php echo $data['D']; ?></label>
                                                </div>
                                                <?php 
                                                 if ($data['E'] != "") {
                                                    ?>
                                                    <div class="option" style="padding-left: 15px;">
                                                        <input type="checkbox" name="<?php echo $questions[$j]['id'] ?>[]" value="D" id="D" />
                                                        <label for="E"><?php echo $data['E']; ?></label>
                                                    </div>
                                                    <?php
                                                }
                                                ?>
                                                 
                                                </td>
                                            </tr>

                                        </tbody>
                                    </table>
                                    <button style="float: right;" type="button"  class="btn btn-primary text">Next</button>
                          <button style="float: right; margin-top:20px;" type="submit"  class="btn btn-primary submit">Complete Test</button>
                                    </fieldset>
                                    <?php
                                }


/**************************************** Question Type 3 ***************************************************************************/


                                    if ($questions[$j]['question_type'] == '3'){
                                            ?>
                         <fieldset id="fieldset_<?php echo $j;?>">
                                      <table style="width:100%">
                                        <tbody style="width:100%">
                                            <tr>
                                            &nbsp;
                                                <!--leave it blank-->
                                                <td>
                                          <div class="row" style="width:100%;margin-left:0px;margin-right:0px">
                                              <div class="col-sm-1" style="width:4.3%;padding-right:0px">
                                             Q <?php echo $j+1;?> 
                                              </div>
                                              <div class="col-sm-11" style=" white-space:normal;padding-left:0px;padding-right:0px;">    
                                                    &nbsp; <?php echo $questions[$j]['question']; ?> </br>&nbsp;
                                                 </div>
                                               </div>


                                                 <div class="option" style="padding-left: 15px;">
                                                        <input type="radio" name="<?php echo $questions[$j]['id'] ?>" value="A" id="A" />
                                                        <label for="designercheckbox"><?php echo $data['A']; ?></label>
                                                 </div>
                                                 <div class="option" style="padding-left: 15px;">
                                                        <input type="radio" name="<?php echo $questions[$j]['id'] ?>" value="B" id="B" />
                                                        <label for="backendcheckbox"><?php echo $data['B']; ?></label>
                                                </div>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                            <button style="float: right;" type="button"  class="btn btn-primary text">Next</button>
                  <button style="float: right; margin-top:20px;" type="submit"  class="btn btn-primary submit">Complete Test</button>
                                    </fieldset>
                                            <?php
                                    }

/**************************************** Question Type 4 ***************************************************************************/

                                if ($questions[$j]['question_type'] == '4'){
                                    ?>

                     <fieldset id="fieldset_<?php echo $j;?>">
                                    <table style="width:100%">
                                        <tbody style="width:100%">
                                            <tr>
                                            &nbsp;
                                                <!--leave it blank-->
                                                <td>
                                           <div class="row" style="width:100%;margin-left:0px;margin-right:0px;box-shadow: 0 0 5px #2e2e2e; border: 1px solid #000;margin-bottom: 10px;padding: 10px;">
                                            <div class="col-sm-12">
                                            <p>Directions</p>
                                            </div>
                                             <div class="col-sm-12">
                                            <?php echo $questions[$j]['direc_text'];?>
                                             </div>
                                           </div>
                                               

                                          <div class="row" style="display:block; width:100%;margin-left:0px;margin-right:0px">
                                              <div class="col-sm-1" style="width:4.3%;padding-right:0px">
                                             Q <?php echo $j+1;?> 
                                              </div>
                                              <div class="col-sm-11" style=" white-space:normal;padding-left:0px;padding-right:0px;">
                                                <?php echo $questions[$j]['question']; ?> </br>&nbsp;
                                               </div>
                                              </div>
                                                
                                                 <div class="option" style="padding-left: 15px;">
                                                        <input type="radio" name="<?php echo $questions[$j]['id'] ?>" value="A" id="A" />
                                                        <label for="designercheckbox"><?php echo $data['A']; ?></label>
                                                 </div>
                                                 <div class="option" style="padding-left: 15px;">
                                                        <input type="radio" name="<?php echo $questions[$j]['id'] ?>" value="B" id="B" />
                                                        <label for="backendcheckbox"><?php echo $data['B']; ?></label>
                                                </div>
                                                 <div class="option" style="padding-left: 15px;">
                                                        <input type="radio" name="<?php echo $questions[$j]['id'] ?>" value="C" id="C" />
                                                        <label for="designercheckbox"><?php echo $data['C']; ?></label>
                                                </div>
                                                 <div class="option" style="padding-left: 15px;">
                                                        <input type="radio" name="<?php echo $questions[$j]['id'] ?>" value="D" id="D" />
                                                        <label for="D"><?php echo $data['D']; ?></label>
                                                </div>
                                                <?php 
                                                  if ($data['E'] != "") {
                                                    ?>
                                                    <div class="option" style="padding-left: 15px;">
                                                        <input type="radio" name="<?php echo $questions[$j]['id'] ?>" value="E" id="E" />
                                                        <label for="D"><?php echo $data['E']; ?></label>
                                                    </div>
                                                    <?php
                                                }
                                                else
                                                {

                                                }
                                                ?>
                                             
                                                </td>
                                            </tr>

                                        </tbody>
                                    </table>
                                                <button style="float: right;" type="button"  class="btn btn-primary text">Next</button>
									  <button style="float: right; margin-top:20px;" type="submit"  class="btn btn-primary submit">Complete Test</button>
                                      
                                </fieldset>
                                    <?php
                                }




                              ?>      
                              <?php 
                            }
                            ?> 
                                        <input type="text" name="num" id="num" value="1">



<!-- <p>  <math><msqrt><mi>2</mi></msqrt></math> </p> -->

                                        <input type="text" name="esd" id="esd" value="<?php echo $i;?>" >

                           <!--<button style="float: right; margin-top:20px;" type="submit"  class="btn btn-primary submit">Complete Test</button>-->
                    
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>
</html>
<style type="text/css">

table
{
    border-collapse: separate;
}


fieldset {
    display: none;
}
#fieldset_0
{
    display: block;
}
<!--.submit
{
    display: none;
}-->

</style>
<script type="text/javascript">
    
$(document).ready(function($){

/*     $(".submit").on("click", function() {

       $(".submit").html('<i class="fa fa-refresh fa-lg fa-spin" style="color: #ffffff;"></i>');
	    $(".submit").confirm({
        text: "Are you sure?",
        title: "Confirmation required",
        confirm: function() {
            $("#message_detail").trigger("submit");  
        },
        cancel: function() {
            //do something
        },
        confirmButton: "Yes",
        cancelButton: "No",
        post: true
    });

    }); */
	var viewedcounter =0;
	$(".linkspan").on("click", function() {
		var numberOfCheckedRadio = $('input:radio:checked').length;
		//alert(numberOfCheckedRadio);
		$('#checkedanswer').val(numberOfCheckedRadio);
		//alert("im here again");
		var i = $('#num').val();
		var k = $('#esd').val();
		var currId = $(this).attr('id');
		var ret = currId.split("_");
		var str1 = ret[0];
		var str2 = ret[1];
		alert(str2);
		alert(i);
		$(".submit").css('display','block');
		if(i!=str2 || str2==1 || str2==15 ){
			var mytarget = str2 - 1 ;
			var oldtarget = i-1;
			viewedcounter++;
			alert(viewedcounter);
			$('#viewedtotal').val(viewedcounter);
			if(viewedcounter ==0){
			
			$("#notvisited").val(15);
			}
			else{
				var notvisited = k-viewedcounter;
			$("#notvisited").val(notvisited);
			}
			$("#fieldset_"+mytarget).css('display','block');
			$("#fieldset_"+oldtarget).css('display','none');
			i++;
			document.getElementById('num').value = str2;
			$("#"+currId).css("background", "yellow");
			$(".text").css('display','block');
		}
	});

	$(".text").on("click", function() {
          var i = document.getElementById('num').value;
          var k = document.getElementById('esd').value;
		  if(parseInt(i) <= parseInt(k)) {
			var numberOfCheckedRadio = $('input:radio:checked').length;
			$('#checkedanswer').val(numberOfCheckedRadio);
			viewedcounter++;
			alert(viewedcounter);
			$('#viewedtotal').val(viewedcounter);
			if(viewedcounter ==0){
			
			$("#notvisited").val(15);
			}
			else{
				var notvisited = k-viewedcounter;
			$("#notvisited").val(notvisited);
			}
			$("#currentbtn_"+i).css("background", "yellow");
			if(i != 0)
			var j = i - 1 ;

			$("#fieldset_"+i).css('display','block');
			$("#fieldset_"+j).css('display','none');

			i++;
			document.getElementById('num').value = i;
		  }
	});

/*     $(".text").on("click", function() {

          alert("im here");
      
          var i = document.getElementById('num').value;
          var k = document.getElementById('esd').value;
          
        
            if(parseInt(i) < parseInt(k)) {
              if(i != 0)
              var j = i - 1 ;

              $("#fieldset_"+i).css('display','block');
              $("#fieldset_"+j).css('display','none');

              i++;
              document.getElementById('num').value = i;
              
              
                timelimit = maxtimelimit;
                clearInterval(myVar);
                startTimer();

          }

          if(parseInt(i) == parseInt(k))
          {
            $(".submit").css('display','block');
            $(".text").css('display','none');
          }
    }); */
});


</script>
<script type="text/javascript">
  
/******************* TIMER *******************/

var maxtimelimit = 60, timelimit = 60;  // 20 seconds per question
var myVar;
function startTimer() {

  myVar = setInterval(function(){myTimer()},1000);
  timelimit = maxtimelimit;
  
}
function myTimer() {
  if (timelimit > 0) {
    curmin=Math.floor(timelimit/60);
    cursec=timelimit%60;
    if (curmin!=0) { curtime=curmin+" minutes and "+cursec+" seconds left"; }
              else { curtime=cursec+" seconds left"; }

    $('#timeleft').html(curtime);
  } else {
    
      clearInterval(myVar);
      var i = document.getElementById('num').value;
      var k = document.getElementById('esd').value;
      if(parseInt(i) < parseInt(k)) {
        if(i != 0)
          var j = i - 1 ;
          $("#fieldset_"+i).css('display','block');
          $("#fieldset_"+j).css('display','none');
          i++;
          document.getElementById('num').value = i;
          var k = document.getElementById('esd').value;
          timelimit = maxtimelimit;
          clearInterval(myVar);
          startTimer();
        }

          if(parseInt(i) == parseInt(k))
          {
            $(".submit").css('display','block');
            $(".text").css('display','none');
            timelimit = maxtimelimit;
         
        //  $.delay(29000, function(){
        //    $("#message_detail").trigger("submit");  
        // });

         setTimeout( function(){ 
           $("#message_detail").trigger("submit");  
         },60000);


          }
        
  }
  timelimit--;
}

var maxtimelimit_new = 900, timelimit_new = 900;  // 20 seconds per question
var myVar_new;
function startTimer_new() {

  myVar_new = setInterval(function(){myTimer_new()},1000);
  timelimit_new = maxtimelimit_new;
  
}
function myTimer_new() {
  if (timelimit_new > 0) {
    curmin=Math.floor(timelimit_new/60);
    cursec=timelimit_new%60;
    if (curmin!=0) { curtime=curmin+" minutes and "+cursec+" seconds left"; }
              else { curtime=cursec+" seconds left"; }
    $('#timeleft_new').html(curtime);
  } else {
    
    clearInterval(myVar_new);

  }
  timelimit_new--;
}



</script>
