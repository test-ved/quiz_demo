<html lang="en">
@section('htmlheader')
    @include('adminlte::layouts.partials.htmlheader')
@show
<script language ="javascript" >
        $(document).keydown(function(e){
         e.preventDefault();
        });


        $(function() {
            $(this).bind("contextmenu", function(e) {
                e.preventDefault();
            });
        });

</script>
<body class="skin-blue sidebar-mini">
<div class="container-fluid">
        <div class="row">
            <div class="col-md-11 content-boxes col-md-offset-0" style="margin-left: 4%; margin-top: 5%">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <div class="form form-horizontal">
                           <div class="div-test-initiator" id="divInitiator">
                                 <div class="mx-none" id="divStartTestInstruction">
                                    <div class="div-test-instruction">
                                            <h1 style="text-align: center; color: green;">Thank You </h1>
                                            <h2 style="text-align: center; color: green;">We will contact you soon.</h2>
                                             <div style="text-align: center; color: green;" id="timeleft" style="float: left;"></div>
                                    </div>
                                 </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">



    $(document).ready(function($){

        setTimeout( function(){ 

            window.close();

         },3000);
    });

    </script>
</body>
</html>

