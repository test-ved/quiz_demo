<html lang="en">
@section('htmlheader')
    @include('adminlte::layouts.partials.htmlheader')
@show
<body class="skin-blue sidebar-mini">

<div class="container-fluid">
        <div class="row">
            <div class="col-md-11 content-boxes col-md-offset-0" style="margin-left: 4%; margin-top: 5%">
                <div class="panel panel-default">
                    <div class="panel-heading" style="background-color: #367fa9; color: white;">Assessment Test

<p style="float: right;"><?php print_r($user['email']);?></p>


                    </div>
                    <div class="panel-body">
                        <div class="form form-horizontal">
                           <div class="div-test-initiator" id="divInitiator">
                                 <div class="mx-none" id="divStartTestInstruction">
                                    <div class="div-test-instruction">
                                            <p class="mx-green mx-bold mx-uline">Instructions:</p>
                                                <ul class="ul-test-instruction">
                                                <li>Total number of questions : <b>15</b>.</li>
                                                <li>Time alloted : <b>15</b> minutes.</li>
                                                <li>Each question carry 1 mark, no negative marks.</li>
                                                <li>DO NOT refresh the page.</li>
                                            </ul>
                                    </div>
                                    <a href="#" id="full_sc" onclick="fullscreenPop()" style="margin-left: 47%;"> 
<!-- 									 <a href="{{url('/assessment/user/start_test')}}" style="margin-left: 47%;"> -->	
                                        <button  style="font-size: 15px;" type="button" class="btn btn-primary">Start Test</button>
                                    </a>
                                  <!--   <input type="button" value="click to toggle fullscreen" onclick="toggleFullScreen()"> -->
                                 </div>
                            </div>
                        </div>
                        <!-- <span class="mandatory">Fields marked with an asterisk (*) are mandatory</span> -->
                    </div>
                </div>
            </div>
        </div>
    </div>

 <script type="text/javascript">

// function fullscreenPop() {
// var URL = "http://www.assessment.in/assessment/user/start_test";
// window.open(URL,'','fullscreen,toolbar=no,scrollbars=1,menubar=no,resizeable=no');
// $("#full_sc").hide();
// $(location).attr('href','http://www.assessment.in/assessment/user/test_started');
// }
function fullscreenPop() {
var URL = "http://quiz.codeslab.in/assessment/user/start_test";
window.open(URL,'','fullscreen,toolbar=no,scrollbars=1,menubar=no,resizeable=no');
$("#full_sc").hide();
$(location).attr('href','http://quiz.codeslab.in/assessment/user/test_started');
}


</script>


</body>
</html>

