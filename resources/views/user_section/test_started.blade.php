<html lang="en">
@section('htmlheader')
    @include('adminlte::layouts.partials.htmlheader')
@show
<body class="skin-blue sidebar-mini">

<div class="container-fluid">
        <div class="row">
            <div class="col-md-11 content-boxes col-md-offset-0" style="margin-left: 4%; margin-top: 5%">
                <div class="panel panel-default">
                    <div class="panel-heading" style="background-color: #367fa9; color: white;">Assessment Test

<p style="float: right;"><?php print_r($user['email']);?></p>


                    </div>
                    <div class="panel-body">
                        <div class="form form-horizontal">
                           <div class="div-test-initiator" id="divInitiator">
                                 <div class="mx-none" id="divStartTestInstruction">
                                    <div style="text-align: center;" class="div-test-instruction">
                                            <p style="text-align: center;" class="mx-green mx-bold mx-uline">Your test Had started. Please Do not switch Test window</p>

                                            <a style="margin-left: auto; margin-right: auto; " href="{{ url('logout') }}"><button class="btn btn-primary">Sign Out</button></a>
                                    </div>
                                 </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
   </div>
</body>
</html>