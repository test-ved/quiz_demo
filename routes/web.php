<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/','HomeController@login');
Route::group(['middleware' => 'auth'], function () {
Route::get('/unauthenticate', 'UserController@get404');
Route::get('/questions', 'QuestionController@index')->middleware('admin');
Route::get('/questions/create', 'QuestionController@create')->middleware('admin');
Route::post('/questions/store', 'QuestionController@store')->middleware('admin');
Route::get('/assessment/result/{id}', 'QuestionController@get_result')->middleware('admin');
Route::get('/assessment/user/assessment_test', 'UserController@user_dashboard')->middleware('user');
Route::get('/assessment/user/start_test', 'UserController@test_view')->middleware('user');;
Route::get('/user_list', 'UserController@user_list');
Route::get('/add_user', 'UserController@add_user')->middleware('admin');
Route::post('/add_user', 'UserController@add_user')->middleware('admin');
Route::post('/assessment/user/test_submit', 'UserController@test_submit')->middleware('user');
Route::get('/assessment/user/test_started', 'UserController@test_started');
Route::get('/assessment/user/user_view', 'UserController@user_view')->middleware('admin');
Route::get('/assessment/user/user_edit', 'UserController@user_edit')->middleware('admin');
Route::post('/assessment/user/user_edit', 'UserController@user_submit')->middleware('user');
Route::get('/assessment/user/user_delete', 'UserController@user_delete')->middleware('admin');
Route::get('/logout', 'UserController@getLogout');
});
// Route::get('/exception/read', function(){ 
// 	return view('errors/tech-error');
// }); 